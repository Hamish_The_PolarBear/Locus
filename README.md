# locus

`This project is pointless since I finally discovered MIDI Monster https://midimonster.net/ exists so I am going to learn to use that rather than re-invent the wheel and probably do a much shoddier job.`

The Locus of Control

A set of Python scripts for universal interaction between control devices and applications.

**Handlers** are scripts that interpret the I/O for a particular device or
application and create Locus commands for that device.

---

main.py is the central script which calls the others and brings everything
together with a dispatcher loop. It interperets Locus commands and sends
responses to handlers as necessary.

e.g. A button press on a physical device might translate as the Play button for
a media player application.

---

Locus Commands (❗WIP)

Locus commands are data packets with a name and a set of parameters attached.

**Read only**

| Command | Parameter(s) | Description |
|---------|--------------|-------------|
| `But_On` | `button` | `button(name) has been pressed` |
| `But_Off` | `button` | `button(name) has been released` |


**Read/Write**

| Command | Parameter(s) | Description |
|---------|--------------|-------------|
| `But_Ind` | `button`,`indicator` | `button(name) with an indicator(on/off)` |
