#!/usr/bin/env python3

# OBS Control Module

# Libraries used
import obswebsocket, obswebsocket.requests
import sys
import time

host = "localhost"
port = 4444
# password = ""
from password import *

password = obswspass

websock = obswebsocket.obsws(host,port,password)

# Connect to an OBS websocket session
def obsconnect(host,port,password):
    websock.connect()
    print("Connected to OBS, Websocket version: ",
websock.call(obswebsocket.requests.GetVersion()).getObsWebsocketVersion())

# Disconnect session
def obsdisconnect():
    websock.disconnect()
    print("Disconnected from OBS")

def obsEvent(message):
    print("message:", message)
    websock.register(obsEvent)


# Test loop
obsconnect(host, port, password)

x = input("press RETURN")

obsdisconnect()
