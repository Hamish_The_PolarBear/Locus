# Data definitions for Behringer X-Touch in "raw" xctl mode over hardware midi (*not* USB)

# Heartbeat message (needs to be sent at least every 7 or 8 seconds to keep link alive)
hb_msg_type = 'sysex'
heartbeat = (0x00, 0x00, 0x66, 0x14, 0x00)

# X-Touch sends a message every few seconds to show it is still alive
alive = (0x00, 0x20, 0x32, 0x58, 0x54, 0x00)

# Scribble Strips
# All scribble strip messages are sysex type and use a common header for the data

scrstr_msgtype = 'sysex'
scrstr_header = (0x00, 0x00, 0x66, 0x58)

# Scribble strips are indexed from hex 20-27
scrstr = [0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27]

# Scribble Strip Colours
scrstr_off = 0x00
scrstr_red = 0x01
scrstr_red_inv = 0x41
scrstr_green = 0x02
scrstr_green_inv = 0x42
scrstr_yellow = 0x03
scrstr_yellow_inv = 0x43
scrstr_blue = 0x04
scrstr_blue_inv = 0x44
scrstr_magenta = 0x05
scrstr_magenta_inv = 0x45
scrstr_cyan = 0x06
scrstr_cyan_inv = 0x46
scrstr_white = 0x07
scrstr_white_inv = 0x47

# Scribble Strip Characters
# Each line is 7 bytes
scrstr_1_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_2_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_3_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_4_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_5_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_6_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_7_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_8_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]

scrstr_1_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_2_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_3_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_4_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_5_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_6_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_7_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
scrstr_8_line1 = [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]

# Faders
# Fader position is set/read by pitchwheel (pitch bend) value on each channel

fad_msg_type = 'pitchwheel'

# -8192 is -inf dB | 4396 is 0dB | 8191 is +10dB

fad_min = -8192
fad_zero = 4396
fad_max = 8188

# Channels 1 to 8 are the channel faders and 9 is the Main

fad_ch = [0,1,2,3,4,5,6,7,8]

# Faders are touch sensitive, notes from 104 to 112 are triggered
# As well as the note being triggered the current value of the 
# fader (pitchwheel) is returned after the note_on message

fad_touch = [104,105,106,107,108,109,110,111,112]

# Endless Rotary Encoders
# Push action is a note value, rotation sends a cc value with acceleration
# measured by repeat movement raising or lowering the value

rot_push = [32,33,34,35,36,37,38,39]
rot_turn = [16,17,18,19,20,21,22,23]

# These are minimum values for rotation - if you rotate faster then the number increases
# TODO: Acceleration details
rot_cw = 1
rot_ccw = 65

# Each rotary indicator has 6 segments on the left (ccw), one centre segment and 6 right side(cw) segments
# cc values 48-55 set the left and centre, 56-63 set the right side. The leds are set by using a bitmap  (6 bits for
# left, 1 bit for centre (on the same bitmap as left) and 6 bits for right).
# The values can be combined to create the necessary pattern or LEDs can be lit individually using the single value.

rot_led_msg_type = 'control_change'

rot_l_led = [48,49,50,51,52,53,54,55]
rot_r_led = [56,57,58,59,60,61,62,63]

# The bitmaps are "backwards" from how they appear on the dial.

rot_led_l6 = 0b0000001
rot_led_l5 = 0b0000010
rot_led_l4 = 0b0000100
rot_led_l3 = 0b0001000
rot_led_l2 = 0b0010000
rot_led_l1 = 0b0100000
rot_led_c0 = 0b1000000
rot_led_r1 = 0b000001
rot_led_r2 = 0b000010
rot_led_r3 = 0b000100
rot_led_r4 = 0b001000
rot_led_r5 = 0b010000
rot_led_r6 = 0b100000


# Shuttle wheel
# Simply returns a value for clockwise and one for anti-clockwise (no acceleration value and no settings)

shut_msg_type = 'control_change'

# The wheel is assigned to cc 60
shut_ch = 60

shut_r = 1
shut_l = 65

## Button LEDs are toggled with a note value
but_led_on = 127
but_led_off = 0
but_led_flash = 1

# Function Buttons - Note values
# Read to detect a press, write to enable LED

func = [54,55,56,57,58,59,60,61]

# Channel Strip Buttons
rec = [0,1,2,3,4,5,6,7]

solo = [8,9,10,11,12,13,14,15]

mute = [16,17,18,19,20,21,22,23]

select = [24,25,26,27,28,29,30,31]

# Encoder Assign Buttons
track = 40
send = 41
pan = 42
plugin = 43
eq = 44
inst = 45

# Miscellaneous

fadbnk_left = 46
fadbnk_right = 47
channel_left = 48
channel_right = 49

flip = 50

global_view = 51

display = 52
smpte = 53

midi_tracks = 62
inputs = 63
audio_tracks = 64
audio_inst = 65
aux = 66
buses = 67
outputs = 68
user = 69

# Modify 
shift = 70
option = 71
control = 72
alt = 73

# Automation
read_off = 74
write = 75
trim = 76
touch = 77
latch = 78
group = 79

# Utility
save = 80
undo = 81
cancel = 82
enter = 83

# Transport
marker = 84
nudge = 85
cycle = 86
drop = 87
replace = 88
click = 89
solo = 90
rew = 91
fwd = 92
stop = 93
play = 94
record = 95

# Cursor
curs_up = 96
curs_down = 97
curs_left = 98
curs_right = 99
search = 100

scrub = 101

# Time display LEDs
solo_led = 115
smpte_led = 113
beats_led = 114
